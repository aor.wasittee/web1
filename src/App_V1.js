
import Footer from './components/Footer';
import Header from './components/Header';
import Logo from './components/Logo';

import './App.css';
import Login from './components/Login';
import Sidebar from './components/Sidebar';
import Menu from './components/Menu';


function App() {
  return (
    <div className = 'App'>

      <br/><br/><br/><br/><br/><br/>

      <Logo></Logo>

      <Header></Header>

      <Login></Login>

      <Footer title = 'Mwit' website = 'mwit.ac.th' postcode = {73170} isOpen></Footer>
      <hr/>

      <Sidebar></Sidebar>
      <hr/>
      <Menu></Menu>
    
    </div>
  );
}

export default App;
