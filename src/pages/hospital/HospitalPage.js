import React from 'react'
import { Table, Spinner } from 'react-bootstrap'
import Pagination from "react-js-pagination";
import axios from 'axios'

const pagesize = 10

const HospitalPage = () => {

    const [hospital, setHospital] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)

    //เก็บค่าปัจจุบันไม่ขึ้นกับ  render
    const cancelToken = React.useRef(null)

    //Pagination
    const [page, setPage] = React.useState(1)
    const [total, setTotal] = React.useState(0)

    const getData = async function (page) {
        try {
            setLoading(true)
            const response = await axios.get(

                `https://api.codingthailand.com/api/hospital2?page=${page}&page_size=${pagesize}`,
                {
                    cancelToken: cancelToken.current.token
                });
            //console.log(response.data.data)
            setHospital(response.data.data)
            setTotal(response.data.meta.pagination.total)
        } catch (error) {
            //console.log(error)
            setError(error)


        } finally {
            setLoading(false)
        }

    }

    React.useEffect(() => {
        cancelToken.current = axios.CancelToken.source()
        getData(page)

        return () => {
            //console.log('exit product page')
            cancelToken.current.cancel()
        }

    }, [page])

    if (loading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                <p>{error.response.data.message} </p>
            </div>
        )
    }
    const handlePageChange = (pageNumber) => {

        setPage(pageNumber)

    }


    return (
        <div>
            <div className='container'>
                
                    <h3>รายงาน</h3>
                    <div className='container'>
                        <div className='row mt-4'>
                            <div className='col-md-12'>
                                <Table striped bordered hover size='sm'>
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>รหัส</th>
                                            <th>ชื่อสถานพยาบาล</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            hospital.map((h, index) => {  // loop

                                                return (
                                                    <tr key={h.id}>
                                                        <td>{h.id}</td>
                                                        <td>{h.code}</td>
                                                        <td>{h.h_name}</td>

                                                    </tr>

                                                )

                                            })
                                        }
                                    </tbody>
                                </Table>
                                <br />

                                <Pagination
                                    activePage={page}
                                    itemsCountPerPage={pagesize}
                                    totalItemsCount={total}
                                    pageRangeDisplayed={20}
                                    onChange={handlePageChange}
                                    itemClass='page-item'
                                    linkClass='page-link'
                                    prevPageText='ก่อนหน้า'
                                    nextPageText='ต่อไป'
                                    firstPageText='หน้าแรก'
                                    lastPageText='หน้าสุดท้าย'

                                />
                            </div>
                        </div>
                    </div>
                </div>
        </div >

    )
}

export default HospitalPage
