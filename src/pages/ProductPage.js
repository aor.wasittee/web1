import React from 'react'
import { Table, Image, Badge, Spinner } from 'react-bootstrap'
import axios from 'axios'
import { format } from 'date-fns'
import { th } from 'date-fns/locale'
import { BsEyeFill } from "react-icons/bs"
import { Link } from "react-router-dom"



const ProductPage = () => {

    const [product, steProduct] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)

    //เก็บค่าปัจจุบันไม่ขึ้นกับ  render
    const cancelToken = React.useRef(null)

    const getData = async () => {
        try {
            setLoading(true)
            const response = await axios.get('https://api.codingthailand.com/api/course', {
                cancelToken: cancelToken.current.token
            });
            //console.log(response.data.data)
            steProduct(response.data.data)
        } catch (error) {
            //console.log(error)
            setError(error)


        } finally {
            setLoading(false)
        }

    }

    React.useEffect(() => {
        cancelToken.current = axios.CancelToken.source()
        getData()

        return () => {
            //console.log('exit product page')
            cancelToken.current.cancel()
        }

    }, [])

    if (loading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                <p>{error.response.data.message} </p>
            </div>
        )
    }


    return (
        <div>
            <div className='container'>
                <div className='row mt-4'>
                    <div className='col-md-12'>
                        <h3>รายการห้องประชุม</h3>
                        <Table striped bordered hover width='100%'>
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>ชื่อ</th>
                                    <th>รายละเอียด</th>
                                    <th>วันที่</th>
                                    <th>views</th>
                                    <th>รูปภาพ</th>
                                    <th>เครื่องมือ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    product.map((p, index) => {  // loop

                                        return (
                                            <tr key={p.id}>
                                                <td>{p.id}</td>
                                                <td>{p.title}</td>
                                                <td>{p.detail}</td>
                                                <td>
                                                    {
                                                        format(new Date(p.date), 'dd/MMM/yyyy', { locale: th })
                                                    }
                                                </td>
                                                <td>
                                                    <Badge variant="success" >{p.view}</Badge>
                                                </td>
                                                <td>
                                                    <Image src={p.picture} alt={p.title} width={100} thumbnail  ></Image>
                                                </td>
                                                <td>
                                                    <Link to={`/detail/${p.id}/title/${p.title}`}>
                                                        <BsEyeFill size={30} />
                                                    </Link>

                                                </td>
                                            </tr>

                                        )

                                    })
                                }
                            </tbody>
                        </Table>

                    </div>
                </div>

            </div>

        </div>
    )
}

export default ProductPage

