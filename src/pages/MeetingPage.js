import React from 'react'
import {
    CardDeck,
    Card,
    Button,
    Spinner
} from 'react-bootstrap'
import { BsFillReplyAllFill, BsChevronDoubleRight, BsCardList } from "react-icons/bs"
import { Link } from "react-router-dom"
import image from '../image/brand-mwit.png'
import image3_0 from '../image/3_0.jpg'
import image14_0 from '../image/14_0.jpg'
import image6_0 from '../image/6_0.jpg'
import image16_0 from '../image/16_0.jpg'
import image4_0 from '../image/4_0.jpg'
import axios from 'axios'
import { useHistory } from "react-router-dom";



const MeetingPage = () => {

    const [Meeting, steMeetingPage] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)

    const history = useHistory();

    //เก็บค่าปัจจุบันไม่ขึ้นกับ  render
    const cancelToken = React.useRef(null)

    const getData = async () => {
        try {
            setLoading(true)
            const response = await axios.get('http://127.0.0.1:5000/api/roommeeting');

            console.log(response.data.[0]._id)
            steMeetingPage(response.data)
        } catch (error) {
            //console.log(error)
            setError(error)


        } finally {
            setLoading(false)


        }

    }


    React.useEffect(() => {
        cancelToken.current = axios.CancelToken.source()
        getData()

        return () => {
            //console.log('exit product page')
            cancelToken.current.cancel()
        }

    }, [])

    if (loading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                <p>{error.response.data.message} </p>
            </div>
        )
    }


    return (
        <div>
            <div className='container'>
                <div className='row mt-4'>
                    <div className='col-md-12'>
                        <h3>รายการห้องประชุม</h3>

                        <Link to='/meeting/freeMeeting' className="btn btn-info btn-sm" role="button" >
                            <h5><BsCardList size={20} />&nbsp;&nbsp; ตรวจสอบห้องว่าง <BsChevronDoubleRight color='white' size={20} /></h5>
                        </Link>




                        <div className='row mt-4'>
                            <CardDeck>
                                <Card>
                                    <Card.Img variant="top"
                                        src={image3_0}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo" />
                                    <Card.Body>
                                        <Card.Title>หอประชุมพระอุบาลีคุณูปมาจารย์</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง : 900 ที่นั่ง

                                            {/* {Meeting.[0]._id} */

                                            }





                                        </Card.Text>
                                        {/* to={`/meeting/detailMeeting/${Meeting.[0]._id}`} */}

                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[0]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}



                                        {/* <Link to={`/meeting/detailMeeting/${Meeting.[0]._id}`} className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={image14_0}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องฉายภาพยนตร์สามมิติ</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	300 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[1]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}

                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={image}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องด้านข้างหอประชุมพระอุบาลีคุณูปมาจารย์</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	50 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[2]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top"
                                        src={image}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo" />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุม A</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	10 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[3]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>


                            </CardDeck>
                        </div>


                        <div className='row mt-4'>
                            <CardDeck>

                                <Card>
                                    <Card.Img variant="top" src={image6_0}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุม B</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	32 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[4]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={image}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุม C</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	60 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[5]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={image16_0}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุม D (2501)</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	10 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[6]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top"
                                        src={image4_0}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo" />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุม ดร.โกวิท วรพิพัฒน์</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	200 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[7]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>


                            </CardDeck>

                        </div>

                        <div className='row mt-4'>
                            <CardDeck>

                                <Card>
                                    <Card.Img variant="top" src={image}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุม ศ.ดร.ณัฐ ภมรประวัติ</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	250 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[8]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={image}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุม ศ.ดร.สิปปนนท์ เกตุทัศน์</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	39 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[9]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={image}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุมCo1_ศุนย์วิทยบริการ</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	10 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[10]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>
                                <Card>
                                    <Card.Img variant="top" src={image}
                                        width="100"
                                        height="170"
                                        className="d-inline-block align-center"
                                        alt="React Bootstrap logo"
                                    />
                                    <Card.Body>
                                        <Card.Title>ห้องประชุมอาคาร12</Card.Title>
                                        <Card.Text>
                                            จำนวนที่นั่ง :	20 ที่นั่ง
                                        </Card.Text>
                                        <Button className="btn btn-warning btn-sm" role="button"

                                            onClick={() => history.push('/meeting/detailMeeting/' + Meeting.[10]._id)}

                                        >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Button>{' '}
                                        {/* <Link to='/meeting/detailMeeting' className="btn btn-warning btn-sm" role="button" >
                                            รายละเอียดเพิ่มเติม <BsFillReplyAllFill color='white' size={20} />
                                        </Link> */}
                                    </Card.Body>
                                    <Card.Footer>
                                        {/* <small className="text-muted">Last updated 3 mins ago</small> */}
                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                            จองห้องประชุม <BsFillReplyAllFill color='white' size={20} />
                                        </Link>
                                    </Card.Footer>
                                </Card>

                            </CardDeck>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default MeetingPage
