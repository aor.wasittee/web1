import React from 'react'
import { Container, Form, Button, Row, Col } from 'react-bootstrap'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import axios from 'axios'
import { useHistory, useParams } from 'react-router-dom'


// กำหนดรูปแบบการคีย์ข้อมูล
const schema = yup.object().shape({
    name: yup.string().required('กรุณากรอก ข้อมูลหมวดหมู่ข่าว'),
});


const EditPage = () => {

    const history = useHistory()
    const { id } = useParams()

    const { register, handleSubmit, errors , setValue  } = useForm({
        resolver: yupResolver(schema)
    });

    // const getData = async (id) => {

    //     const resp = await axios.get(
    //         'https://api.codingthailand.com/api/category/' + id)
    //     console.log(resp.data)
    //     setValue('name', resp.data.name) //แสดงชื่อ

    // }

    // React.useEffect(() => {
    //     console.log('useEffect')
    //     getData(id)
    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [id])
    // cast  React.useCallback  warning hidden
    const getData = React.useCallback (async () => {

        const resp = await axios.get(
            'https://api.codingthailand.com/api/category/' + id)
        console.log(resp.data)
        setValue('name', resp.data.name) //แสดงชื่อ
    
    }, [id, setValue])

    React.useEffect(() => {
        console.log('useEffect')
        getData()   
    }, [getData])


    const onSubmit = async (data) => {
        //console.log(data)
        //ส่งข้อมูลไปที่ server แบบ put
        const apiUrl = 'https://api.codingthailand.com/api/category'
        const resp = await axios.put(apiUrl, {
            id: id,
            name: data.name
        })
        alert(resp.data.message)
        history.replace('/category')
    }


    return (
        <div>
            <Container className='mt-4'>
                <Row>
                    <Col xs={12} md={8}>
                        <h1>เพิ่มข้อมูล</h1>
                        <Form onSubmit={handleSubmit(onSubmit)}>
                            <Form.Group controlId="name">
                                <Form.Label><h5>หมวดหมู่ข่าว</h5></Form.Label>
                                <Form.Control type="text" name='name' ref={register} className={`form.control ${errors.name ? 'is-invalid' : ''}`} />
                                {
                                    errors.name && (
                                        <Form.Control.Feedback type="invalid">
                                            <h6>{errors.name.message}</h6>
                                        </Form.Control.Feedback>
                                    )
                                }
                            </Form.Group>

                            <Button variant="primary" type="submit" onClick=''>
                                บันทึก
                </Button>
                        </Form>
                        <hr />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default EditPage
