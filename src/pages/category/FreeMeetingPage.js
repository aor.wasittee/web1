import React from 'react'
import {
    Table,
    Image, Badge,
    Spinner, Form, Row, Col, Button, InputGroup, FormControl
} from 'react-bootstrap'
import axios from 'axios'
import { format } from 'date-fns'
import { th } from 'date-fns/locale'
import { BsCalendar, BsClock, BsArrowClockwise, BsChevronDoubleRight } from "react-icons/bs"
import { FaCheckCircle, FaTimesCircle, FaInfoCircle } from "react-icons/fa";
import { Link, useHistory } from 'react-router-dom'


const FreeMeetingPage = () => {
    const history = useHistory()

    const [freemeeting, steFreeMeetingRoom] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)

    //เก็บค่าปัจจุบันไม่ขึ้นกับ  render
    const cancelToken = React.useRef(null)

    const getData = async () => {
        try {
            setLoading(true)
            const response = await axios.get('http://127.0.0.1:5000/api/meeting', {
                cancelToken: cancelToken.current.token
            });

            //console.log(response.data.data)
            //steFreeMeetingRoom(response.data.data)

            response.data[0]['status'] = 'ว่าง'
            response.data[1]['status'] = 'ไม่ว่าง'
            response.data[2]['status'] = 'ไม่ว่าง'
            response.data[3]['status'] = 'ว่าง'

            //console.log(response.data)
            steFreeMeetingRoom(response.data)

        } catch (error) {
            //console.log(error)
            setError(error)


        } finally {
            setLoading(false)
        }

    }

    React.useEffect(() => {
        cancelToken.current = axios.CancelToken.source()
        getData()

        return () => {
            //console.log('exit product page')
            cancelToken.current.cancel()
        }

    }, [])

    if (loading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                <p>{error.response.data.message} </p>
            </div>
        )
    }

    return (
        <div>
            <div>
                <div className='container'>
                    <div className='row mt-4'>
                        <div className='col-md-12'>
                            <h3>ตรวจสอบห้องว่าง</h3>
                            <Row >
                                <Col xs={12} md={3}>
                                    <Form.Group as={Row} controlId="date">
                                        <Form.Label >
                                            วันที่
                                    </Form.Label>
                                        <Col sm={10}>
                                            <Form.Control type="date" placeholder="date" />
                                        </Col>
                                    </Form.Group>
                                </Col>
                                <Col xs={12} md={6}>
                                    <Form.Group as={Row} controlId="room">
                                        <Form.Label >
                                            เวลา
                                    </Form.Label>
                                        <Col sm={4}>
                                            <InputGroup className="mb-2 mr-sm-2">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text><BsClock size={15} /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="time" id="time_s" />
                                            </InputGroup>
                                        </Col>
                                        ถึง
                                        <Col sm={4}>
                                            <InputGroup className="mb-2 mr-sm-2">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text><BsClock size={15} /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="time" id="time_e" />
                                            </InputGroup>
                                        </Col>
                                    </Form.Group>
                                </Col>

                                <Col xs={12} md={3}>
                                    <Form.Group as={Row} controlId="date">
                                        <Col sm={6}>
                                            <Button variant="secondary mt-2" onClick={() => {
                                                history.goBack()
                                            }} size="sm" >&nbsp;&nbsp; คลิก &nbsp;&nbsp;<BsArrowClockwise color='white' size={20} /></Button>
                                        </Col>
                                    </Form.Group>
                                </Col>

                            </Row>

                            <Table striped bordered hover width='100%' >
                                <thead >
                                    <tr>
                                        <th>ลำดับที่</th>
                                        <th>ชื่อห้อง</th>
                                        <th>จำนวนที่นั่ง</th>
                                        <th>สถานะห้อง </th>
                                        <th>รายละเอียด</th>
                                        {/* <th>แก้ไข/ลบ</th> */}
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        freemeeting.map((p, index) => {  // loop

                                            return (
                                                <tr key={p.id}>
                                                    <td>{p.id}</td>
                                                    <td>{p.room}<br></br>{p.detail}</td>
                                                    {/* <td><Badge variant="success" >{p.view}</Badge></td> */}

                                                    <td>
                                                        {/* <Badge variant="success" >{p.view}</Badge> */}
                                                    </td>

                                                    <td>
                                                        {p.status}
                                                        {/* <Button className='ml-2' variant='success' size='sm'

                                                        >
                                                            <FaCheckCircle /> ว่าง
                                                    </Button>{' '}


                                                        <Button className='ml-2' variant='danger' size='sm'

                                                        >
                                                            <FaTimesCircle /> ไม่ว่าง
                                                    </Button>{' '} */}


                                                        {/* <Image src={p.picture} alt={p.title} width={100} thumbnail  ></Image> */}
                                                    </td>
                                                    <td>
                                                        <Link to='/meeting/createMeeting' className="btn btn-primary btn-sm" role="button" >
                                                            จองห้องประชุม <BsChevronDoubleRight color='white' size={20} />
                                                        </Link>


                                                    </td>
                                                    {/* <td>
                                                   
                                                <Button className='ml-2' variant='primary' size='sm' 
                                                    
                                                    >
                                                    <BsPencil /> แก้ไขข้อมูล
                                                    </Button>{' '}
                                                  

                                                    <Button className='ml-2' variant='danger' size='sm'
                                                    
                                                    >
                                                    <BsTrash/> ลบข้อมูล
                                                    </Button> 

                                                </td> */}

                                                    {/* <td>
                                                    <Image src={p.picture} alt={p.title} width={100} thumbnail  ></Image>
                                                </td> */}
                                                    {/* <td>
                                                    <Link to={`/detail/${p.id}/title/${p.title}`}>
                                                        <BsEyeFill size={30} />
                                                    </Link>

                                                </td> */}
                                                </tr>

                                            )

                                        })
                                    }
                                </tbody>
                            </Table>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    )
}

export default FreeMeetingPage
