import React from 'react'
import { Table, Spinner, Button } from 'react-bootstrap'
import axios from 'axios'
import { BsPencil, BsTrash } from "react-icons/bs"
import { useHistory } from 'react-router-dom'


const IndexPage = () => {

    const [category, setCategory] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)

    const history = useHistory() //เปิดหน้าใหม่

    //เก็บค่าปัจจุบันไม่ขึ้นกับ  render
    const cancelToken = React.useRef(null)


    const getData = async function () {
        try {
            setLoading(true)
            const response = await axios.get(

                `https://api.codingthailand.com/api/category`,
                {
                    cancelToken: cancelToken.current.token
                });
            //console.log(response.data.data)
            setCategory(response.data)

        } catch (error) {
            //console.log(error)
            setError(error)


        } finally {
            setLoading(false)
        }

    }

    React.useEffect(() => {
        cancelToken.current = axios.CancelToken.source()
        getData()

        return () => {
            //console.log('exit product page')
            cancelToken.current.cancel()
        }

    }, [])

    if (loading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                <p>{JSON.stringify(error)} </p>
            </div>
        )
    }



    return (
        <div>
            <h2>หมวดหมู่ข่าว</h2>
            <div className='container'>
                <div className='row mt-4'>
                    <div className='col-md-12'>
                        <Button 
                        className='mb-3'
                        variant='success'
                        onClick={() => history.push('/category/create')} >
                            เพิ่มข้อมูล
                        </Button>
                        <Table striped bordered hover size='sm'>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>หมวดหมู่ข่าว</th>
                                    <th>เครื่องมือ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    category.map((c, index) => {  // loop

                                        return (
                                            <tr key={c.id}>
                                                <td>{c.id}</td>
                                                <td>{c.name}</td>
                                                <td>
                                                    <Button className='ml-2' variant='outline-info' size='sm' 
                                                    onClick={() => history.push('/category/edit/' + c.id) }
                                                    >
                                                    <BsPencil /> แก้ไขข้อมูล
                                                    </Button>
                                                    <Button className='ml-2' variant='outline-danger' size='sm'
                                                    onClick={ async () => {
                                                        const isConfirm = window.confirm('แน่ใจว่าต้องการลบข้อมูล '+c.name +' ? ')
                                                        if(isConfirm === true){
                                                            const resp = await axios.delete('https://api.codingthailand.com/api/category/'+ c.id)
                                                            alert(resp.data.message)
                                                            history.go(0) // ref f5
                                                        }
                                                        
                                                    }}
                                                    >
                                                    <BsTrash/> ลบข้อมูล
                                                    </Button>
                                                </td>

                                            </tr>

                                        )

                                    })
                                }
                            </tbody>
                        </Table>

                    </div>
                </div>
            </div>
        </div>

    )
}

export default IndexPage
