import React from 'react'
import { Table, Image, Badge, Spinner, Button } from 'react-bootstrap'
import { BsFillReplyAllFill } from "react-icons/bs"
import { useHistory ,useParams } from 'react-router-dom'
import axios from "axios";
import Moment from 'moment';
///

const DetailMeetingPage = () => {

    const history = useHistory()
    const { _id } = useParams()


    const [detail_m, setDetailM] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)
    const cancelToken = React.useRef(null)


    const getData = async (_id) => {


        try {
            setLoading(true);
            const resp = await axios.get(
                "http://127.0.0.1:5000/api/roommeeting/" + _id,
                {
                    cancelToken: cancelToken.current.token,
                }
            );
            console.log(resp.data)
            setDetailM(resp.data);
        } catch (error) {
            console.log(error);
            setError(error);
        } finally {
            setLoading(false);
        }
    };

    React.useEffect(() => {
        cancelToken.current = axios.CancelToken.source();

        getData(_id);

        return () => {
            // console.log('exit product page')
            cancelToken.current.cancel();
        };
    }, [_id]);

    if (loading === true) {
        return (
            <div className="text-center mt-5">
                <Spinner animation="border" variant="primary" />
            </div>
        );
    }

    if (error) {
        return (
            <div className="text-center mt-5 text-danger">
                <p>เกิดข้อผิดพลาดจาก Server กรุณาลองใหม่</p>
                <p>{error.response.data.message}</p>
            </div>
        );
    }


    return (
        <div>

            <div className='container'>
                <div className='row mt-4'>
                    <div className='col-md-12'>
                        <h3>รายละเอียดเพิ่มเติม</h3>
                        <Table striped bordered hover width='100%'>
                            <thead>
                                <tr>
                                    <th width='20%' variant="dark" className="text-right table-active">ชื่อห้องประชุม : </th>
                                    <th><index type='text' name="room_name" >{detail_m.room_name}</index></th>
                                </tr>
                                <tr>
                                    <th width='20%' variant="dark" className="text-right table-active">จำนวนที่นั่ง : </th>
                                    <th>{detail_m.num_room}</th>
                                </tr>
                                <tr>
                                    <th width='20%' variant="dark" className="text-right table-active">รายละเอียดอื่นๆ : </th>
                                    <th>{detail_m.detail}</th>
                                </tr>
                                <tr>
                                    <th width='20%' variant="dark" className="text-right table-active">จองล่วงหน้า : </th>
                                    <th>{detail_m.book}</th>
                                </tr>
                                <tr>
                                    <th width='20%' variant="dark" className="text-right table-active">ชื่อผู้รับผิดชอบอุปกรณ์ : </th>
                                    <th>{detail_m.user}</th>
                                </tr>
                                <tr>
                                    <th width='20%' variant="dark" className="text-right table-active">e-mail  : </th>
                                    <th>{detail_m.email}</th>
                                </tr>


                            </thead>
                           
                        </Table>
                        &nbsp;&nbsp;&nbsp;&nbsp;   
                        <Button variant="secondary mt-2" onClick={() => {
                                history.goBack()
                            }} size="lg" >&nbsp;&nbsp; ย้อนกลับ &nbsp;&nbsp;<BsFillReplyAllFill color='white' size={20} /></Button>

                    </div>
                </div>

            </div>
        </div>
    )
}

export default DetailMeetingPage
