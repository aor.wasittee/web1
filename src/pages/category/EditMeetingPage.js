import React from 'react'
import { Container, Form, Button, Row, Col, InputGroup, FormControl } from 'react-bootstrap'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import axios from 'axios'
import { useHistory, useParams } from 'react-router-dom'
import {
    BsPlusCircleFill, BsPersonFill, BsCaList,
    BsPencil, BsCardText, BsCalendar,
    BsClock, BsFillReplyAllFill, BsFileEarmarkPlus
} from "react-icons/bs"
import { AiFillPhone } from "react-icons/ai"


// กำหนดรูปแบบการคีย์ข้อมูล
const schema = yup.object().shape({
    name: yup.string().required('กรุณากรอก ข้อมูลหมวดหมู่ข่าว'),
});


const EditMeetingPage = () => {

    const history = useHistory()
    const { _id } = useParams()

    
    // const { register, handleSubmit, formState: { errors } } = useForm({
    //     resolver: yupResolver(schema)
    // });

    const { register, handleSubmit, errors, setValue } = useForm({
        resolver: yupResolver(schema),
      });


    const getData = React.useCallback(async () => {
        const resp = await axios.get(
            "http://127.0.0.1:5000/api/meeting/" + _id)
        console.log(resp.data)
        //console.log(resp.data.room)
       setValue(
        "name", resp.data.name
       )
        
    }, [_id, setValue])

    React.useEffect(() => {
        // console.log('useEffect edit page')
        getData();
    }, [getData]);



    const onSubmit = async (data) => {
        //console.log(data)
        //ส่งข้อมูลไปที่ server แบบ post
        // const apiUrl = 'https://api.codingthailand.com/api/category'
        const apiUrl = 'http://127.0.0.1:5000/api/meeting'
        const resp = await axios.put(apiUrl, {
            id: _id,
            name: data.name,
          })
        alert(resp.data.message) //แก้ไขข้อมูลเรียบร้อย
        history.replace('/meeting')
    }


    return (
        <div>
            <Container className='mt-4'>
                <h3>จองห้องประชุม</h3>

                <Form onSubmit={handleSubmit(onSubmit)} >

                    <Row className='mt-5'>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="room">
                                <Col sm={2}>
                                    <Form.Label >
                                        ชื่อห้อง
                                    </Form.Label>
                                </Col>
                                <Col sm={10}>
                                    <Form.Control as="select" id="room" name='room'>
                                        <option value="">--เลือก--</option>
                                        <option value="หอประชุมพระอุบาลีคุณูปมาจารย์">หอประชุมพระอุบาลีคุณูปมาจารย์</option>
                                        <option value="ห้องฉายภาพยนตร์สามมิติ">ห้องฉายภาพยนตร์สามมิติ</option>
                                        <option value="ห้องด้านข้างหอประชุมพระอุบาลีคุณูปมาจารย์">ห้องด้านข้างหอประชุมพระอุบาลีคุณูปมาจารย์</option>
                                        <option value="ห้องประชุม 1212 (E)">ห้องประชุม 1212 (E)</option>
                                        <option value="ห้องประชุม A">ห้องประชุม A</option>
                                        <option value="ห้องประชุม B">ห้องประชุม B</option>
                                        <option value="ห้องประชุม C">ห้องประชุม C</option>
                                        <option value="ห้องประชุม D (2501)">ห้องประชุม D (2501)</option>
                                        <option value="ห้องประชุม ดร.โกวิท วรพิพัฒน์">ห้องประชุม ดร.โกวิท วรพิพัฒน์</option>
                                        <option value="ห้องประชุม ศ.ดร.ณัฐ ภมรประวัติ">ห้องประชุม ศ.ดร.ณัฐ ภมรประวัติ</option>
                                        <option value="ห้องประชุม ศ.ดร.สิปปนนท์ เกตุทัศน์">ห้องประชุม ศ.ดร.สิปปนนท์ เกตุทัศน์</option>
                                        <option value="ห้องสตูดิโอ">ห้องสตูดิโอ</option>
                                    </Form.Control>
                                    {/* <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsCardList size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl id="room" />
                                    </InputGroup> */}
                                    {/* <Form.Control type="text" /> */}
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="num_room">
                                <Col sm={4}>
                                    <Form.Label >
                                        จำนวนผู้เข้าประชุม
                                    </Form.Label>
                                </Col>
                                <Col sm={8}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsPersonFill size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="text" id="num_room" name="num_room"/>
                                    </InputGroup>
                                    {/* <Form.Control type="text" /> */}
                                </Col>
                            </Form.Group>
                        </Col>

                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="title">
                                <Col sm={2}>
                                    <Form.Label >
                                        หัวข้อ
                                    </Form.Label>
                                </Col>
                                <Col sm={10}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsPencil size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="text" id="title" name="title" ref={register}/>
                                    </InputGroup>
                                    {/* <Form.Control type="text" /> */}
                                </Col>
                            </Form.Group>
                        </Col>


                    </Row>

                    <Row>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="name">
                                <Col sm={2}>
                                    <Form.Label >
                                        ชื่อผู้จอง
                                    </Form.Label>
                                </Col>
                                <Col sm={10}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsPersonFill size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="text" id="name" name="name" ref={register}/>
                                    </InputGroup>
                                    {/* <Form.Control type="text" /> */}
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="tel">
                                <Col sm={2}>
                                    <Form.Label >
                                        เบอร์โทร
                                    </Form.Label>
                                </Col>
                                <Col sm={6}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><AiFillPhone size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="text" id="tel" name="tel"/>
                                    </InputGroup>
                                    {/* <Form.Control type="text" /> */}
                                </Col>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="use">
                                <Col sm={2}>
                                    <Form.Label >
                                        ใช้สำหรับ
                                    </Form.Label>
                                </Col>
                                <Col sm={10}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsCardText size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="text" id="use" name="use"/>
                                    </InputGroup>
                                    {/* <Form.Control type="text" /> */}
                                </Col>
                            </Form.Group>
                        </Col>

                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="date_s">
                                <Col sm={4}>
                                    <Form.Label >
                                        วันที่เริ่มต้น/เวลาเริ่มต้น
                                    </Form.Label>
                                </Col>
                                <Col sm={6}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsCalendar size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="date" id="date_s" name="date_s"/>
                                    </InputGroup>
                                    {/* <Form.Control type="date" /> */}
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="time_s">

                                <Col sm={6}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsClock size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="time" id="time_s" name="time_s"/>
                                    </InputGroup>
                                    {/* <Form.Control type="time" /> */}
                                </Col>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="date_e">
                                <Col sm={4}>
                                    <Form.Label >
                                        วันที่สิ้นสุด/เวลาสิ้นสุด
                                    </Form.Label>
                                </Col>
                                <Col sm={6}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsCalendar size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="date" id="date_e" name="date_e" />
                                    </InputGroup>
                                    {/* <Form.Control type="date" /> */}
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col xs={12} md={6}>
                            <Form.Group as={Row} controlId="time_e">

                                <Col sm={6}>
                                    <InputGroup className="mb-2 mr-sm-2">
                                        <InputGroup.Prepend>
                                            <InputGroup.Text><BsClock size={15} /></InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <FormControl type="time" id="time_e" name="time_e"/>
                                    </InputGroup>
                                    {/* <Form.Control type="time" /> */}
                                </Col>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={12}>
                            <Form.Group as={Row} controlId="">
                                <Col sm={6}>
                                    <Form.Label >
                                        อุปกรณ์
                                    </Form.Label>
                                </Col>
                            </Form.Group>
                        </Col>

                    </Row>

                    <Row>
                        {['checkbox'].map((type) => (
                            <div key={`custom-inline-${type}`} className="mb-3">
                                <Col sm={12}>
                                    <Form.Check
                                        custom
                                        inline
                                        label="คอมพิวเตอร์(Notebook)"
                                        type={type}
                                        id={`${type}_1`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="เครื่องฉาย Projector"
                                        type={type}
                                        id={`${type}_2`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="เครื่องฉายแผ่นใส"
                                        type={type}
                                        id={`${type}_3`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="เครื่องขยายเสียง"
                                        type={type}
                                        id={`${type}_4`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="เครื่องฉายภาพ 3 มิติ"
                                        type={type}
                                        id={`${type}_5`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ขอสายเสียงต่อคอมพิวเตอร์"
                                        type={type}
                                        id={`${type}_6`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="กล้อง"
                                        type={type}
                                        id={`${type}_7`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ไมโครโฟน 1 ตัว"
                                        type={type}
                                        id={`${type}_8`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ไมโครโฟน 2 ตัว"
                                        type={type}
                                        id={`${type}_9`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ไมค์ลอย 1 ตัวแบบมือถือ"
                                        type={type}
                                        id={`${type}_10`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ไมค์ลอย 2 ตัวแบบมือถือ"
                                        type={type}
                                        id={`${type}_11`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ไมค์ลอย 1 ตัวแบบติดเสื้อ"
                                        type={type}
                                        id={`${type}_12`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ไมค์ลอย 1 ตัวแบบเหน็บหู"
                                        type={type}
                                        id={`${type}_13`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="ระบบชุดไมค์ประชุม"
                                        type={type}
                                        id={`${type}_14`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="โทรทัศน์ LED TV 55 นิ้ว"
                                        type={type}
                                        id={`${type}_15`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="โทรทัศน์ LED TV 65 นิ้ว"
                                        type={type}
                                        id={`${type}_16`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="จัดห้องแบบห้องเรียน"
                                        type={type}
                                        id={`${type}_17`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="จัดห้องแบบ Theater"
                                        type={type}
                                        id={`${type}_18`}
                                    />
                                    <Form.Check
                                        custom
                                        inline
                                        label="จัดห้องแบบตัว U"
                                        type={type}
                                        id={`${type}_19`}
                                    />




                                </Col>
                            </div>
                        ))}

                    </Row>

                    <Row>
                        <Col xs={12} md={12}>
                            <Form.Group as={Row} controlId="">
                                <Col sm={6}>
                                    <Form.Label >
                                        อื่นๆ
                            </Form.Label>
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col xs={12} md={12}>
                            <Form.Group controlId="other">
                                <Form.Control as="textarea" rows={4} />
                            </Form.Group>
                        </Col>
                    </Row>


                    <Button variant="primary mt-2" type="submit" size="lg" >
                        <BsFileEarmarkPlus color='white' size={20} />&nbsp;&nbsp; บันทึก &nbsp;&nbsp;&nbsp;&nbsp;
                    </Button>
                    {' '}&nbsp;&nbsp;&nbsp;&nbsp;
                    <Button variant="secondary mt-2" onClick={() => {
                        history.goBack()
                    }} size="lg" >&nbsp;&nbsp; ย้อนกลับ &nbsp;&nbsp;<BsFillReplyAllFill color='white' size={20} /></Button>

                </Form>
                <hr />

            </Container>
        </div >
    )
}

export default EditMeetingPage
