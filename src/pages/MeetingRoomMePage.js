import React from 'react'
import {
    Table,
    //  Image, Badge,
    Spinner, Form, Row, Col, Button
} from 'react-bootstrap'
import axios from 'axios'
import { format } from 'date-fns'
import { th } from 'date-fns/locale'
import { BsPencil, BsTrash } from "react-icons/bs"
import { FaCheckCircle, FaTimesCircle, FaInfoCircle } from "react-icons/fa";
//import { Link } from "react-router-dom"
import { If, Then, ElseIf, Else } from 'react-if-elseif-else-render';
import { useHistory } from "react-router-dom";


const MeetingRoomMePage = () => {

    const [meetingroomMe, steMeetingRoomMe] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)

    const history = useHistory();

    //เก็บค่าปัจจุบันไม่ขึ้นกับ  render
    const cancelToken = React.useRef(null)

    const getData = async () => {
        try {
            setLoading(true)
            const response = await axios.get('http://127.0.0.1:5000/api/meeting');

            response.data[0]['status'] = 'รอตรวจสอบ'
            response.data[1]['status'] = 'อนุมัติ'
            response.data[2]['status'] = 'ยกเลิก'
            response.data[3]['status'] = 'รอตรวจสอบ'



            //console.log(response.data.data)
            //steMeetingRoomMe(response.data.data)

            //console.log(response.data[0])
            steMeetingRoomMe(response.data)
        } catch (error) {
            //console.log(error)
            setError(error)


        } finally {
            setLoading(false)
        }

    }


    React.useEffect(() => {
        cancelToken.current = axios.CancelToken.source()
        getData()

        return () => {
            //console.log('exit product page')
            cancelToken.current.cancel()
        }

    }, [])

    if (loading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                <p>{error.response.data.message} </p>
            </div>
        )
    }


    return (
        <div>
            <div className='container'>
                <div className='row mt-4'>
                    <div className='col-md-12'>
                        <h3>รายการจองห้องประชุมของฉัน</h3>
                        <Row >
                            <Col xs={12} md={3}>
                                <Form.Group as={Row} controlId="date">
                                    <Form.Label >
                                        วันที่
                                    </Form.Label>
                                    <Col sm={10}>
                                        <Form.Control type="date" placeholder="date" />
                                    </Col>
                                </Form.Group>
                            </Col>
                            <Col xs={12} md={6}>
                                <Form.Group as={Row} controlId="room">
                                    <Form.Label >
                                        ห้องประชุม
                                    </Form.Label>
                                    <Col sm={10}>
                                        <Form.Control as="select">
                                            <option key={0}>--เลือก--</option>
                                            <option key={1}>หอประชุมพระอุบาลีคุณูปมาจารย์</option>
                                            <option key={2}>ห้องฉายภาพยนตร์สามมิติ</option>
                                            <option key={3}>ห้องด้านข้างหอประชุมพระอุบาลีคุณูปมาจารย์</option>
                                            <option key={4}>ห้องประชุม 1212 (E)</option>
                                            <option key={5}>ห้องประชุม A</option>
                                            <option key={6}>ห้องประชุม B</option>
                                            <option key={7}>ห้องประชุม C</option>
                                            <option key={8}>ห้องประชุม D (2501)</option>
                                            <option key={9}>ห้องประชุม ดร.โกวิท วรพิพัฒน์</option>
                                            <option key={10}>ห้องประชุม ศ.ดร.ณัฐ ภมรประวัติ</option>
                                            <option key={11}>ห้องประชุม ศ.ดร.สิปปนนท์ เกตุทัศน์</option>
                                            <option key={12}>ห้องสตูดิโอ</option>

                                        </Form.Control>
                                    </Col>
                                </Form.Group>
                            </Col>
                            <Col xs={12} md={3}>
                                <Form.Group as={Row} controlId="status">
                                    <Form.Label >
                                        สถานะ
                                    </Form.Label>
                                    <Col sm={10}>
                                        <Form.Control as="select">
                                            <option key={0}>--เลือก--</option>
                                            <option key={1}>อนุมัติ</option>
                                            <option key={2}>รอตรวจสอบ</option>
                                            <option key={3}>ยกเลิก</option>

                                        </Form.Control>
                                    </Col>
                                </Form.Group>
                            </Col>
                        </Row>

                        <Table striped bordered hover width='100%' >
                            <thead >
                                <tr>
                                    <th>ลำดับที่</th>
                                    <th>ชื่อห้อง</th>
                                    <th>ชื่อผู้จอง</th>
                                    <th>เบอร์โทร</th>
                                    <th>วันที่จอง</th>
                                    <th>สถานะ</th>
                                    {/* <th>รายละเอียด</th> */}
                                    <th>แก้ไข/ลบ</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    meetingroomMe.map((p, index) => {  // loop

                                        return (
                                            <tr key={p._id}>
                                                <td>{p._id}</td>
                                                <td>{p.room}<br></br>
                                                    หัวข้อการประชุม
                                                    {p.title}</td>
                                                <td>{p.detail}</td>

                                                <td>
                                                    {/* {p.name} */}
                                                </td>
                                                <td>{Date(p.start) - p.end}</td>
                                                <td>
                                                    <If condition={p.status == "อนุมัติ"}>
                                                        <Then>
                                                            <p> <Button className='ml-2' variant='success' size='sm'

                                                            >
                                                                <FaCheckCircle /> อนุมัติ
                                                                </Button></p>
                                                        </Then>


                                                        <ElseIf condition={p.status == "รอตรวจสอบ"}>
                                                            <p><Button className='ml-2' variant='warning' size='sm'

                                                            >
                                                                <FaInfoCircle /> รอตรวจสอบ
                                                                </Button></p>
                                                        </ElseIf>
                                                        <Else>
                                                            <p><Button className='ml-2' variant='danger' size='sm'

                                                            >
                                                                <FaTimesCircle /> ยกเลิก
                                                                </Button></p>
                                                        </Else>
                                                    </If>
                                                    {/* {p.status} */}
                                                    {/* <Image src={p.picture} alt={p.title} width={100} thumbnail  ></Image> */}
                                                </td>

                                                <td>

                                                    <Button className='ml-2' variant='primary' size='sm'

                                                        onClick={() => history.push('/edit/' + p._id)}

                                                    >
                                                        <BsPencil /> แก้ไขข้อมูล
                                                    </Button>{' '}


                                                    <Button className='ml-2' variant='danger' size='sm'
                                                        onClick={async () => {
                                                            const isConfirm = window.confirm('แน่ใจว่าต้องการลบข้อมูล ' + p.room + '?')
                                                            if (isConfirm === true) {
                                                                const resp = await axios.delete('http://127.0.0.1:5000/api/meeting/' + p._id)
                                                                alert(resp.data.message)
                                                                history.go(0)
                                                                //history.replace('/meetingroomMe')
                                                            }
                                                        }}

                                                    >
                                                        <BsTrash /> ลบข้อมูล
                                                    </Button>

                                                </td>

                                                {/* <td>
                                                    <Image src={p.picture} alt={p.title} width={100} thumbnail  ></Image>
                                                </td> */}
                                                {/* <td>
                                                    <Link to={`/detail/${p.id}/title/${p.title}`}>
                                                        <BsEyeFill size={30} />
                                                    </Link>

                                                </td> */}
                                            </tr>

                                        )

                                    })
                                }
                            </tbody>
                        </Table>

                    </div>
                </div>

            </div>

        </div>
    )
}

export default MeetingRoomMePage

