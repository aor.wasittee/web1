import React from 'react'
import { Col, Row, Container, Button, Form } from 'react-bootstrap'
import { useForm } from 'react-hook-form'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import { useToasts } from 'react-toast-notifications'

const SUPPORTED_IMAGE_FORMATS = ["image/jpg", "image/jpeg"];

const UploadPage = () => {

    const history = useHistory()
    const { addToast } = useToasts()
    // handleSubmit  = select data
    const { handleSubmit, errors, register } = useForm()

    const onSubmit = (data) => {
        //console.log(data)
        // แปลข้อมูลให้เป็น   64 
        try {

            let fileUpload = data.picture[0]
            const render = new FileReader()   //  แปลง 64  
            render.readAsDataURL(fileUpload)
            render.onload = async (e) => {

                let base64Image = e.target.result
                const urlApi = 'https://api.codingthailand.com/api/upload'
                const resp = await axios.post(urlApi, {
                    picture: base64Image

                })
                //alert(resp.data.data.message)
                //console.log(resp.data.data.url)
                addToast(resp.data.data.message, { appearance: 'success' });
                history.replace('/')
            }

        } catch (error) {
            console.log(error)
            addToast(JSON.stringify(error), { appearance: 'error' });

        }

    }


    return (
        <Container className='mt-4'>
            <Row>
                <Col md={4}>
                    <h1> UPload Image</h1>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-group">
                            <label htmlFor="exampleFormControlFile1">
                                Example file input
                            </label>
                            <input
                                type="file"
                                name="picture"
                                ref={register({
                                    required: "กรุณาเลือกไฟล์ภาพก่อน",
                                    validate: {
                                        checkFileType: (value) => {
                                            return (
                                                value && SUPPORTED_IMAGE_FORMATS.includes(value[0].type)
                                            )
                                        }
                                    }
                                })}
                                className={`form-control-file ${errors.picture ? "is-invalid" : ""
                                    }`}
                                id="exampleFormControlFile1"
                            />
                            {errors.picture && errors.picture.type === "required" && (
                                <Form.Control.Feedback type="invalid">
                                    <h6>{errors.picture.message}</h6>
                                </Form.Control.Feedback>

                            )}

                            {errors.picture && errors.picture.type === "checkFileType" && (
                                <Form.Control.Feedback type="invalid">
                                    <h6>ไฟล์ภาพรองรับเฉพาะนามสกุล .jpg หรือ jpeg เท่านั้น</h6>
                                </Form.Control.Feedback>

                            )}

                        </div>

                        <Button className='bnt btn-primary' type='submit'>Upload...</Button>
                        <hr />
                    </form>
                </Col>
            </Row>
        </Container>
    )
}

export default UploadPage


