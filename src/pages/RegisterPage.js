import React from 'react'
import { Container, Form, Button, Row, Col } from 'react-bootstrap'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import { useToasts } from 'react-toast-notifications'

// กำหนดรูปแบบการคีย์ข้อมูล
const schema = yup.object().shape({
    name: yup.string().required('กรุณากรอก ชื่อ-สกุล'),
    email: yup.string().required('กรุณากรอก E-mail'),
    password: yup.string().required('กรุณากรอก รหัสผ่าน').min(3, 'รหัสผ่าน 3 ตัวขึ้นไป'),
});



const RegisterPage = () => {

    const history = useHistory()
    const { addToast } = useToasts()

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schema)
    });

    const onSubmit = async (data) => {
        try {
        console.log(data)
        //ส่งข้อมูลไปที่ server แบบ post
        const apiUrl = 'https://api.codingthailand.com/api/register'
        const resp = await axios.post(apiUrl, {
            name : data.name,
            email : data.email,
            password : data.password,

        })
        //alert(resp.data.message)
        addToast(resp.data.message, { appearance: 'success' });
        history.replace('/login')
            
        } catch (error) {

             addToast(error.response.data.errors.email[0], { appearance: 'error' });
        }
    }


    return (
        <div>
            <Container className='mt-4'>
                <Row>
                    <Col xs={12} md={8}>
                        <h1>ลงทะเบียน</h1><p/>
                        <Form onSubmit={handleSubmit(onSubmit)}>
                            <Form.Group controlId="name">
                                <Form.Label><h5>ชื่อ-สกุล</h5></Form.Label>
                                <Form.Control 
                                type="text" 
                                name='name' 
                                ref={register} 
                                className={`form.control ${errors.name ? 'is-invalid':''}`}/>
                                {
                                    errors.name && (
                                        <Form.Control.Feedback type="invalid">
                                            <h6>{errors.name.message}</h6>
                                        </Form.Control.Feedback>
                                    )
                                }
                            </Form.Group>

                            <Form.Group controlId="email">
                                <Form.Label><h5>e-mail</h5></Form.Label>
                                <Form.Control 
                                type="email" 
                                name='email' 
                                ref={register} 
                                className={`form.control ${errors.email ? 'is-invalid':''}`}/>
                                {
                                    errors.email && (
                                        <Form.Control.Feedback type="invalid">
                                            <h6>{errors.email.message}</h6>
                                        </Form.Control.Feedback>
                                    )
                                }
                            </Form.Group>

                            <Form.Group controlId="password">
                                <Form.Label><h5>รหัสผ่าน</h5></Form.Label>
                                <Form.Control 
                                type="password" 
                                name='password' 
                                ref={register} 
                                className={`form.control ${errors.password ? 'is-invalid':''}`}/>
                                {
                                    errors.password && (
                                        <Form.Control.Feedback type="invalid">
                                            <h6>{errors.password.message}</h6>
                                        </Form.Control.Feedback>
                                    )
                                }
                            </Form.Group>

                            <Button variant="primary" type="submit" onClick=''>
                                สมัครสมาชิก
                </Button>
                        </Form>
                        <hr />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default RegisterPage
