import React from 'react'
import { Container, Form, Button, Row, Col } from 'react-bootstrap'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import { useToasts } from 'react-toast-notifications'
import { BsBoxArrowRight } from "react-icons/bs"

// กำหนดรูปแบบการคีย์ข้อมูล
const schema = yup.object().shape({
    email: yup.string().required('กรุณากรอก E-mail'),
    password: yup.string().required('กรุณากรอก รหัสผ่าน').min(3, 'รหัสผ่าน 3 ตัวขึ้นไป'),
});



const LoginPage = () => {

    const history = useHistory()
    const { addToast } = useToasts()

    const { register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schema)
    });

    const onSubmit = async (data) => {
        try {
        //console.log(data)
        //ส่งข้อมูลไปที่ server แบบ post
        const apiUrl = 'http://127.0.0.1:5000/api/accounts/auth'
        //const apiUrl = "https://api.codingthailand.com/api/login"
        const resp = await axios.post(apiUrl, {
            email : data.email,
            password : data.password,

        })

        
        //console.log(resp.data)
        //localStorage.setItem('token',JSON.stringify(resp.data.data))
        localStorage.setItem('token',JSON.stringify(resp.data))

        //get profile
        const urlProfile = 'http://127.0.0.1:5000/api/accounts/profile'
        //const urlProfile = "https://api.codingthailand.com/api/profile"

        const respProfile = await axios.get(urlProfile, {
            headers : {
                Authorization : 'Bearer ' + resp.data.access_token
            }
            
        })

        //console.log(respProfile.data.data.user)
        localStorage.setItem('profile',JSON.stringify(respProfile.data.data.user))



        //alert(resp.data.message)
        addToast('เข้าสู่ระบบเรียบร้อยแล้ว', { appearance: 'success' });
        history.go(0)
        //history.replace('/')
        
            
        } catch (error) {

             addToast(error.response.data.message, { appearance: 'error' });
        }
  

    }


    return (
        <div>
            <Container className='mt-4'>
                <Row>
                    <Col xs={12} md={8}>
                        <h1>เข้าสู่ระบบ</h1><p/>
                        <Form onSubmit={handleSubmit(onSubmit)}>
                            
                            <Form.Group controlId="email">
                                <Form.Label><h5>e-mail</h5></Form.Label>
                                <Form.Control 
                                type="email" 
                                name='email' 
                                ref={register} 
                                className={`form.control ${errors.email ? 'is-invalid':''}`}/>
                                {
                                    errors.email && (
                                        <Form.Control.Feedback type="invalid">
                                            <h6>{errors.email.message}</h6>
                                        </Form.Control.Feedback>
                                    )
                                }
                            </Form.Group>

                            <Form.Group controlId="password">
                                <Form.Label><h5>รหัสผ่าน</h5></Form.Label>
                                <Form.Control 
                                type="password" 
                                name='password' 
                                ref={register} 
                                className={`form.control ${errors.password ? 'is-invalid':''}`}/>
                                {
                                    errors.password && (
                                        <Form.Control.Feedback type="invalid">
                                            <h6>{errors.password.message}</h6>
                                        </Form.Control.Feedback>
                                    )
                                }
                            </Form.Group>

                            <Button variant="primary" type="submit" onClick=''>
                            <BsBoxArrowRight size={20} />{' '} เข้าสู่ระบบ
                </Button>
                        </Form>
                        <hr />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default LoginPage
