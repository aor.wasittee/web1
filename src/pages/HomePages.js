import React from 'react'
import { BsFillHeartFill } from "react-icons/bs"
import { Link } from "react-router-dom"
import { Spinner} from 'react-bootstrap'

import { useQuery } from 'react-query'


const HomePages = () => {

    // const { isLoading, error, data, isFetching } = useQuery('getData', () =>
    //     fetch('https://api.codingthailand.com/api/news?page=1&per_page=3'
    //     ).then(res =>
    //         res.json()
    //     )
    // )

    //  return  get data
    const query = useQuery('getData', () => {
        const controller = new AbortController()
        const signal = controller.signal

        const promise = fetch('https://api.codingthailand.com/api/news?page=1&per_page=3', {
            method: 'get',
            signal: signal


        }
        ).then(res => res.json())

        //cancel request
        promise.cancel = () => controller.abort()

        return promise

    })

    const { isLoading, error, data, isFetching } = query



    if (isLoading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                <p>{JSON.stringify(error)} </p>
            </div>
        )
    }

    const ShowMe = () => {

        alert('Hello React')
    }

    return (
        <div>
            <main role="main">
                {/* Main jumbotron for a primary marketing message or call to action */}
                <div className="jumbotron">
                    <div className="container">
                        {/* <h5 className="display-5">ยินดีต้อนรับ</h5> */}
                        <p>การเขียน React ระบบจองห้องประชุม  {' '}<BsFillHeartFill color='red' size={20} /></p>

                        <p>
                            
                            {/* <a href='/product' className="btn btn-primary btn-lg" role="button"> สินค้าทั้งหมด »</a> */}

                            {/* <Link to='/product' className="btn btn-primary btn-lg" role="button" >
                                สินค้าทั้งหมด »
                            </Link> */}
                        </p>

                    </div>
                </div>
                {/* <div className="container">
                    <idv className='text-center'>
                        {isFetching ? 'กำลังอัปเดท....' : null}
                    </idv>

                    <div className="row">
                        {
                            data.data.map((news, index) => {
                                return (
                                    <div className='col-md-4' key={news.id}>
                                        <h2>{news.topic}</h2>
                                        <p>
                                            {news.detail}
                                        </p>
                                        <p>
                                            หมวดหมู่ : {news.name}
                                        </p>
                                    </div>
                                )

                            })
                        }
                    </div>

                    <hr />
                </div>  */}
            </main>




        </div>
    )
}

export default HomePages
