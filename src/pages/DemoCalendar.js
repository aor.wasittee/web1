import React from 'react'
import Badge from 'react-bootstrap/Badge'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import listPlugin from '@fullcalendar/list'





 const DemoCalendar = () => {

    const handleDateClick = (dateClickInfo: any, callback:any) => { 
       //retrieveDate(dateClickInfo.dateStr, dateClickInfo.end, callback)  
       alert(dateClickInfo.dateStr)
        console.log(dateClickInfo.dateStr, dateClickInfo.end, callback)

    }
  
    

    return (
        <div>
            <div className='container'>
                <FullCalendar
                    plugins={[dayGridPlugin, interactionPlugin, listPlugin]}
                    initialView="dayGridMonth"
                    dateClick={handleDateClick}
                    selectable={true}
                    selectHelper={true}
                    editable={true}
                    eventLimit={true}
                    headerToolbar={{
                        left: 'prev,next today',
                        center: 'title',
                        right: 'dayGridMonth,dayGridWeek,dayGridDay,listWeek',

                    }}
                    events={[
                        { title: 'event 1', start: '2019-04-01' },
                        { title: 'event 2', start: '2019-04-02' }
                      ]}
                    
                    


                />
            </div>
        </div>
    )
}

export default DemoCalendar
