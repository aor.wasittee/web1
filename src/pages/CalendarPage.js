import React from 'react'
import {Badge, Spinner} from 'react-bootstrap'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import listPlugin from '@fullcalendar/list'
import axios from 'axios'

// const eventsData = [
//     { title: 'event 1', date: '2021-05-18' },
//     { title: 'event 2', date: '2021-05-15' },

// ]

//const eventsData = [{ "_id": "60a339b5e4dd2c826cc97af7", "title": "ประชุม", "room": "ห้องประชุม A", "start": "2021-05-18T08:30:00+00:00", "end": "2021-05-18T09:30:00+00:00" }, { "_id": "60a34114e86b824534c60fdd", "title": "ประชุม", "room": "ห้องประชุม A", "start": "2021-05-18T08:30:00+00:00", "end": "2021-05-18T09:30:00+00:00", "__v": 0 }]


const CalendarPage = () => {

    const [eventsData, setCalendarData] = React.useState([])
    const [loading, setLoading] = React.useState(false)
    const [error, setError] = React.useState(null)

    const getData = async () => {
        try {

            setLoading(true)
        const response = await axios.get('http://127.0.0.1:5000/api/meeting')
        //const response = await axios.get('https://api.codingthailand.com/api/course')
            //console.log(response.data.data)
            //setCalendarData(response.data.data)

            console.log(response.data)
            setCalendarData(response.data)

        } catch (error) {
            //console.log(error)
            setError(error)


        } finally {
            setLoading(false)
        }

    }

    React.useEffect(() => {

        getData()

        return () => {
            //console.log('exit product page')

        }

    }, [])

    if (loading === true) {
        return (
            <div className='text-center mt5' >
                <Spinner animation="border" variant="primary" />
            </div>
        )

    }
    if (error) {
        return (
            <div className='text-center mt5 text-danger' >
                <p>เกิดข้อผิดพลาดจาก  Server error </p>
                {/* <p>{error.response.data.message} </p> */}
            </div>
        )
    }

    const handleDateClick = (dateClickInfo: any, callback: any) => {
        //retrieveDate(dateClickInfo.dateStr, dateClickInfo.end, callback)  
        alert(dateClickInfo.dateStr)
        console.log(dateClickInfo.dateStr, dateClickInfo.end, callback)

    }


    return (
        <div className='container'>
            <div className='row mt-4'>
                <div className='col-md-12'>
                    <h3>ปฏิทินการจองห้องประชุม</h3>

                    <FullCalendar
                        plugins={[dayGridPlugin, interactionPlugin, listPlugin]}
                        initialView="dayGridMonth"
                        selectable={true}
                        selectHelper={true}
                        editable={true}
                        selectMirror={true}
                        dayMaxEvents={true}
                        eventLimit={true}
                       // dateClick={handleDateClick}
                        headerToolbar={{
                            left: 'prev,next today',
                            center: 'title',
                            right: 'dayGridMonth,dayGridWeek,dayGridDay,listWeek',

                        }}


                        events={eventsData}

                    />






                </div>

                {/* <div className='col-md-12 mt-3'>
                    <div>
                        <h5>
                            <Badge variant="success">&nbsp;&nbsp;&nbsp;&nbsp;อนุมัติ&nbsp;&nbsp;&nbsp;&nbsp;</Badge>{' '}
                            <Badge variant="warning">&nbsp;&nbsp;&nbsp;&nbsp;รอตรวจสอบ&nbsp;&nbsp;&nbsp;&nbsp;</Badge>{' '}
                            <Badge variant="danger">&nbsp;&nbsp;&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;&nbsp;&nbsp;</Badge>{' '}
                        </h5>
                    </div>

                </div> */}
            </div>

        </div>
    )
}

export default CalendarPage
