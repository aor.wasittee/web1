import React from 'react'

const Sidebar = () => {

    //let fullname = 'Wasittee'
    // funcetion update fullname  การใช้ state
    const [fullname, setFullname] = React.useState('Wasittee')  
    const [isShow, setIsShow] = React.useState(true)

    const changeName = () => {
        //fullname = 'Musikapan'
        setFullname('Musikapan')
        setIsShow(!isShow)
    }

    React.useEffect(()=>{
        console.log('test')

    })

    React.useEffect(()=>{    ///การใช้งานแค่รอบเดียว
        console.log('test only')

    },[])

    React.useEffect(()=>{    ///ทำงานต่อเมื่อมีการเปลี่ยนค่า  edit
        console.log('test =>' + fullname)

    },[fullname])

    return (
        <div>
            <h3>Sidebar</h3>
            {
                isShow?<p>Hello</p> : <p>Word</p>
            }
            <p>
                สวัสดี    {fullname}
            </p>

            <button onClick={changeName}> เปลี่ยนชื่อ </button>

        </div>
    )
}

export default Sidebar
