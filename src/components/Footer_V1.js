import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import PropTypes from 'prop-types'; 
import image from '../image/brand-mwit.png';

const Footer = ({ title, website,postcode, isOpen }) => {  //props
    //const {title,website,postcode,isOpen} = props;

    return (
        <div>


            <Navbar bg="dark" variant="dark" >
                <Navbar.Brand href="#home">
                    <img
                        alt = ""
                        src= {image}
                        width = "30"
                        height = "30"
                        className = "d-inline-block align-top"
                    />
                    <h3>{title}&copy; {new Date().getFullYear()} </h3>
                    <p>{website} {postcode}  isOpen:{isOpen.toString()}</p>
                </Navbar.Brand>
            </Navbar>



        </div>
    )
}

Footer.propTypes = {
    title: PropTypes.string,
    website:PropTypes.string,
    postcode:PropTypes.number,
    isOpen:PropTypes.bool
  };

export default Footer
