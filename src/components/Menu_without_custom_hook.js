import React from 'react'
import setting from '../image/brand-mwit.png';

const Menu2 = () => {

    const [hover, setHover] = React.useState(false)

    const mouseOver = () => {
        setHover(true)
    }
    const mouseOut = () => {
        setHover(false)
    }



    return (
        <div>
            <h1>Menu</h1>
            {
                hover ? <h3>เมนูหลัก</h3> : null
            }
            <img src={setting} onMouseOver={mouseOver} onMouseOut={mouseOut} alt='logo'></img>
        </div>
    )
}

export default Menu2
