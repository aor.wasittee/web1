import React from 'react'
import {
    Nav,
    Navbar,
    // NavDropdown,
    // Form,
    // FormControl,
    Button
} from 'react-bootstrap'
import {
    NavLink,
    useHistory
} from "react-router-dom";

import image from '../image/brand-mwit.png'
import { BsPersonFill, BsFillHouseFill } from "react-icons/bs"


const NavBar = () => {

    const history = useHistory()
    const [profile, setProfile] = React.useState(null)

    const getProfile = () => {
        const profileValue = JSON.parse(localStorage.getItem('profile'))
        if (profileValue) {
            setProfile(profileValue)
        }

    }

    React.useEffect(() => {
        console.log('user effect navbar')
        getProfile()
    }, [])

    const logout = () => {
        localStorage.removeItem('token')
        localStorage.removeItem('profile')
        history.replace('/')
         history.go(0)
    }

    return (

        <div>
            <Navbar bg="dark" expand="lg" variant="dark" >
                <NavLink className="navbar-brand" to='/' exact>
                    <img
                        src={image}
                        width="50"
                        height="50"
                        className="d-inline-block align-center"
                        alt="React Bootstrap logo"
                    />
                    {' '}<label><h4>ระบบจองห้องประชุม</h4></label>
                </NavLink>
                {/* <Navbar.Brand href="#home">
                    <img

                        src={image}
                        width="50"
                        height="50"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />
                    {' '}<label><h3>ระบบทดสอบ</h3></label>

                </Navbar.Brand> */}

                <Navbar.Toggle aria-controls="basic-navbar-nav" />

                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {/* <NavLink className='nav-link' to='/' exact activeClassName='active'>
                        <Button variant="outline-info" >หน้าแรก</Button>
                        </NavLink> */}
                        <NavLink className='nav-link' to='/calendar' exact activeClassName='active'>
                        <Button variant="outline-info" >   ปฏิทินการจองห้องประชุม</Button> 
                        </NavLink>
                        {/* <NavLink className='nav-link' to='/demoCalendar' exact activeClassName='active'>
                        <Button variant="outline-info" >   ปฏิทิน</Button> 
                        </NavLink> */}
                        <NavLink className='nav-link' to='/meeting' activeClassName='active'>
                        <Button variant="outline-info" >    รายการห้องประชุม</Button>
                        </NavLink>
                        <NavLink className='nav-link' to='/meetingroom' activeClassName='active'>
                        <Button variant="outline-info" >    รายการจองห้องประชุม</Button>
                        </NavLink>
                        <NavLink className='nav-link' to='/meetingroomMe' activeClassName='active'>
                        <Button variant="outline-info" >    รายการจองห้องประชุมของฉัน</Button>
                        </NavLink>

                        {/* <NavLink className='nav-link' to='/product' activeClassName='active'>
                        <Button variant="outline-info" >    รายการจองห้องประชุม</Button>
                        </NavLink> */}

                        {/* <NavLink className='nav-link' to='/hospital' activeClassName='active'>
                        <Button variant="outline-info" >    รายการห้องประชุม</Button>
                        </NavLink> */}
                        {/* <NavLink className='nav-link' to='/' activeClassName='active'>
                         <Button variant="outline-info" >    ออกจากระบบ</Button>
                        </NavLink> */}
                        {/* <NavLink className='nav-link' to='/about' activeClassName='active'>
                         <Button variant="outline-info" >    About</Button>
                        </NavLink> */}




                        {/* <Nav.Link href="#home">Home</Nav.Link> */}
                        {/* <Nav.Link href="#link">About</Nav.Link> */}
                        {/* <NavDropdown
                            title="Workshop (pagination+ CRUD)"
                            id="basic-nav-dropdown">
                            <NavDropdown.Item onClick={() => {
                                history.replace('/hospital')
                            }}>
                                ข้อมูลสถานพยาบาล (Pagination)
                            </NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item onClick={() => {
                                history.replace('/category')
                            }}>
                                หมวดหมู่ข่าว (CRUD)
                            </NavDropdown.Item>
                        </NavDropdown> */}

                        {/* <NavLink className='nav-link' to='/upload' activeClassName='active'>
                            Uploadfile
                        </NavLink> */}

                    </Nav>

                    { profile ? (
                           
                            <span className ='navbar-text text-white'>
                                <BsPersonFill color='white' size={20}  /> {' '}
                                   ยินดีต้อนรับ : {profile.name}
                                    {/* role: {profile.role} */}
                                    {' '}<Button variant="outline-danger" onClick={logout}>ออกจากระบบ</Button>
                            </span>
                          
                        ): (
                                
                        <Nav >
                            {/* <NavLink className = 'nav-link' to = '/register' activeClassName = 'active'>
                            <Button variant="outline-info" >    สมัครสมาชิก</Button>
                            </NavLink> */}
                            <NavLink className='nav-link' to='/login' activeClassName='active'>
                            <Button variant="outline-info "  ><BsFillHouseFill  size={20}  />&nbsp;เข้าระบบ</Button>
                            </NavLink>
                        </Nav>

                        )

                    }
                    {/* <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-info">Search</Button>
                    </Form> */}
                </Navbar.Collapse >

            </Navbar >



        </div >
    )
}

export default NavBar
