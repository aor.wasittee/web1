import React from 'react'
import image from '../image/brand-mwit.png';
const Footer = () => {
    return (
        <div>
            <footer className="container" >
           
            <div className='row mt-4'>
            <div className='col-md-12'>
                <p>
                    <img
                        alt=""
                        src={image}
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />{' '}
                    
                    © www.mwit.ac.th  {(new Date().getFullYear())}</p>
            </div>    
            </div>     
            </footer>

        </div>
    )
}

export default Footer

