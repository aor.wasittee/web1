import React from 'react'
import setting from '../image/brand-mwit.png';
import useHover from '../hooks/UserHover'



const Menu = () => {

    const [hover, attrs] = useHover()    


    return (
        <div>
            <h1>Menu</h1>
            {
                hover ? <h3>เมนูหลัก</h3> : null
            }
            {/* <img {...attrs} src={setting} alt='logo'></img> */}
            <img onMouseOver={attrs.mouseOver} onMouseOut={attrs.mouseOut}  src={setting} alt='logo'></img>
        </div>
    )
}

export default Menu
