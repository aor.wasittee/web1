import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ToastProvider } from 'react-toast-notifications'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import NavBar from './components/NavBar'
import HomePages from './pages/HomePages'
import AboutPage from './pages/AboutPage';
import Footer from './components/Footer'
import ProductPage from './pages/ProductPage';
import DetailPage from './pages/DetailPage';
//import HospitalPage from './pages/hospital/HospitalPage';
import IndexPage from './pages/category/IndexPage';
import CreatePage from './pages/category/CreatePage';
import EditPage from './pages/category/EditPage';
//import UploadPage from './pages/UploadPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import CalendarPage from './pages/CalendarPage';
import MeetingPage from './pages/MeetingPage';
import MeetingRoomPage from './pages/MeetingRoomPage';
import CreateMeetingPage from './pages/category/CreateMeetingPage';
import MeetingRoomMePage from './pages/MeetingRoomMePage';
import DetailMeetingPage from './pages/category/DetailMeetingPage';
import FreeMeetingPage from './pages/category/FreeMeetingPage';
import DetailMeetingRoomPage from './pages/category/DetailMeetingRoomPage';
import DemoCalendar from './pages/DemoCalendar';
import EditMeetingPage from './pages/category/EditMeetingPage';


const queryClient = new QueryClient()

function App() {   //root component
  return (
    <ToastProvider
      placement='top-center'
      autoDismissTimeout={3000}
    >
      <QueryClientProvider client={queryClient}>
        <Router>

          <NavBar />
          <Switch>
            <Route exact path='/'>
              <HomePages />
            </Route>
            <Route path='/calendar'>
              <CalendarPage />
            </Route>
            {/* <Route path='/demoCalendar'>
              <DemoCalendar />
            </Route> */}
            <Route path='/product'>
              <ProductPage />
            </Route>
            <Route path='/meeting'
              render={({ match: { url } }) => (
                <div>
                  <Route path={`${url}/`} exact>
                    <MeetingPage />
                  </Route>
                  <Route path={`${url}/createMeeting`} >
                    <CreateMeetingPage />
                  </Route>
                  <Route path={`${url}/detailMeeting/:_id`} >
                    <DetailMeetingPage />
                  </Route>
                  {/* <Route path={`${url}/detailMeeting/:_id`} >
                    <DetailMeetingPage />
                  </Route> */}
                  <Route path={`${url}/freeMeeting`} >
                    <FreeMeetingPage />
                  </Route>
                  {/* <Route path={`${url}/edit/:id`}>
                  <EditPage />
                </Route> */}
                </div>

              )}


            >
            </Route>

            <Route path='/meetingroom'
              render={({ match: { url } }) => (
                <div>
                  <Route path={`${url}/`} exact>
                    <MeetingRoomPage />
                  </Route>
                  <Route path={`${url}/detailMeetingRoom/:_id`} >
                    <DetailMeetingRoomPage />
                  </Route>

                </div>

              )}
            >
            </Route>


            {/* <Route path='/meetingroom'>
              <MeetingRoomPage />
            </Route> */}
            <Route path='/meetingroomMe'>
              <MeetingRoomMePage />
            </Route>
            {/* <Route path="/detail/:id">
              <DetailPage />
            </Route> */}
            <Route path='/edit/:_id'>
              <EditMeetingPage />
            </Route>

            {/* <Route path='/hospital'>
              <HospitalPage />
            </Route> */}
            {/* <Route path='/upload'>
              <UploadPage />
            </Route> */}
            <Route path='/login'>
              <LoginPage />
            </Route>
            <Route path='/register'>
              <RegisterPage />
            </Route>

            <Route
              path='/category'
              render={({ match: { url } }) => (
                <div>
                  <Route path={`${url}/`} exact>
                    <IndexPage />
                  </Route>
                  <Route path={`${url}/create`} >
                    <CreatePage />
                  </Route>
                  <Route path={`${url}/edit/:_id`}>
                    <EditPage />
                  </Route>
                </div>

              )}

            >
            </Route>

            <Route path='/about'>
              <AboutPage />
            </Route>

          </Switch>
          <Footer />

        </Router>
      </QueryClientProvider>
    </ToastProvider>

  );
}

export default App;
